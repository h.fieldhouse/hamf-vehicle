%% draw_vehicle.m
% *Summary:* Draw the cart-pole system with reward, applied force, and 
% predictive uncertainty of the tip of the pendulum
%
%    function draw_vehicle(x, theta, force, cost, text1, text2, M, S)
%
%
% *Input arguments:*
%
%	y          lateral displacement of car
%   theta      yaw angle of car
%   delta      steering angle input to car
%   cost       cost structure
%     .fcn     function handle (it is assumed to use saturating cost)
%     .<>      other fields that are passed to cost
%   M          (optional) mean of state
%   S          (optional) covariance of state
%   text1      (optional) text field 1
%   text2      (optional) text field 2
%
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified by HAMF: 2021-10-15

function draw_vehicle(y, theta, delta, cost, text1, text2, M, S)
%% Code

Figure = figure(1);
set(Figure, 'MenuBar', 'none');
set(Figure, 'ToolBar', 'none');

a    = 1.4;
b    = 1.6;
w    = 0.4;
xmin = -15;
xmax = 15;
ymin = -10; 
ymax = 10;    
maxU = 1;

% Compute positions 
vehicle_loc = [a*cos(theta), y+a*sin(theta); -b*cos(theta), y-b*sin(theta)];
wheel_rad = [w*cos(theta+delta), w*sin(theta+delta)];
f_wheel_loc = [vehicle_loc(1,:) + wheel_rad; vehicle_loc(1,:) - wheel_rad];
wheel_rad = [w*cos(theta), w*sin(theta)];
r_wheel_loc = [vehicle_loc(2,:) + wheel_rad; vehicle_loc(2,:) - wheel_rad];

clf; hold on
% plot(0,2*l,'k+','MarkerSize',20,'linewidth',2)
% plot([xmin, xmax], [-height-0.03, -height-0.03],'k','linewidth',2)

% Plot target y-value
plot([-100,100],[0,0],'Color','#D3D3D3','linewidth',1)

% Plot steering angle
plot([0 delta/maxU*xmax],[-2, -2],'g','linewidth',10)

% Plot reward
reward = 1-cost.fcn(cost,[0, 0, y, 0]', zeros(4));
plot([0 reward*xmax],[-5, -5],'y','linewidth',10)

% Plot positional error
plot([-14,-14],[0,y],'b','linewidth',10)

% Plot the vehicle
plot(vehicle_loc(:,1), vehicle_loc(:,2),'k','linewidth',2)
plot(f_wheel_loc(:,1), f_wheel_loc(:,2),'r','linewidth',4)
plot(r_wheel_loc(:,1), r_wheel_loc(:,2),'r','linewidth',4)

% plot ellipse around tip of vehicle (if M, S exist)
% try
%   [M1 S1] = getPlotDistr_vehicle(M,S,2*l);
%   error_ellipse(S1,M1,'style','b');
% catch
% end

% Text
text(0,-2.5,'deltaangle')
text(0,-5.5,'immediate reward')
y_label = text(-12.5,-3,'y position');
set(y_label,'Rotation',90)
if exist('text1','var')
  text(0,-8, text1)
end
if exist('text2','var')
  text(0,-9, text2)
end
text(-12,-10.5,['y = ' num2str(y)])
text(-4,-10.5,['theta = ' num2str(theta)])
text(6,-10.5,['delta = ' num2str(delta)])

set(gca,'DataAspectRatio',[1 1 1],'XLim',[xmin xmax],'YLim',[ymin ymax]);
axis off;
drawnow;