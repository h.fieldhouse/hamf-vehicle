%% dynamics_vehicle.m
% *Summary:* Implements ths ODE for simulating the cart-pole dynamics.
%
%    function dz = dynamics_vehicle(t, z, f)
%
%
% *Input arguments:*
%
%   t       current time step (called from ODE solver)
%   z       state                                               [4 x 1]
%   delta   optional): steer angle delta(t)
%
% *Output arguments:*
%
%   dz    if 3 input arguments:      state derivative wrt time
%         if only 2 input arguments: total mechanical energy
%
%
% Note: It is assumed that the state variables are of the following order:
%       v:        [m/s]     sideslip velocity
%       omega:    [rad/s]   yaw velocity
%       y:        [m]       lateral displacement
%       theta:    [rad]     yaw angle
%
%
%
% Last modified by HAMF: 2021-10-13

function dz = dynamics_vehicle(t,z,delta)
%% Code

% Dynamics constants
a = 1.4;        % [m]       CoM to front axle
b = 1.6;        % [m]       CoM to rear axle
m = 1500;       % [kg]      mass of car
k2 = 0.9*a*b;   % [m^2]     radius of gyration squared
Cf = 10e3;      % [N/rad]   front tyre cornering stiffness
Cr = 10e3;      % [N/rad]   rear tyre dornering stiffness
U = 20;         % [m/s]     forward speed

C=Cf+Cr;
S=(a*Cf-b*Cr)/(Cf+Cr);
q2=(a*a*Cf+b*b*Cr)/(Cf+Cr);
I=m*k2;

if nargin==3
  dz = zeros(4,1);
  invM=[1/m 0; 0 1/I];
  K=[C/U (C*S/U)+m*U; C*S/U C*q2/U];
  B=[Cf; a*Cf];
 
  dvw=invM*( -K*z(1:2) + B*delta(t) );
  
  dz(1) = dvw(1);
  dz(2) = dvw(2);
  dz(3) = z(1)*cos(z(4))+U*sin(z(4));    % small angle approximation removed
  dz(4) = z(2);
else
  dz = ( (z(1)+U*z(4))^2 + k2*z(2)*z(2) )*m/2;
end