%% sparsity_simulation.m
% *Summary:* Script to run a dynamics simulation
%
%
% *Input arguments:*
%
%
%
% Last modified by HAMF: 2021-10-18
%
%% High-Level Steps
% # Load parameters
% # Create J initial trajectories by applying random controls

%% Code

% 1. Initialization
clear all; close all;
settings_vehicle;                      % load scenario-specific settings
basename = 'vehicle_';           % filename used for saving data

J = 10;

% 2. Initial J random rollouts
for jj = 1:J
  [xx, yy, realCost{jj}, latent{jj}] = ...
    rollout(gaussian(mu0, S0), struct('maxU',policy.maxU), H, plant, cost);
  x = [x; xx]; y = [y; yy];       % augment training sets for dynamics model
  if plotting.verbosity > 0      % visualization of trajectory
    if ~ishandle(1); figure(1); else set(0,'CurrentFigure',1); end; clf(1);
    draw_rollout_vehicle;
  end
  
end

save([basename 'rand_' num2str(J)])