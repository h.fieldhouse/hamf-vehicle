%% sparsity_simulation.m
% *Summary:* Script to run a dynamics simulation
%
%
%   function sparsity_simulation()
%
% *Input arguments:*
%
%
%
% Last modified by HAMF: 2021-10-18
%
%% High-Level Steps
% # 
% # 
% # 


%% Code

% 1. Initialization
clear all; close all;
settings_vehicle;                      % load scenario-specific settings
basename = 'vehicle_';                 % filename used for saving data

dynmodel.induce = zeros(50,0,1);      % shared inducing inputs (sparse GP)
                                       % adjust sparsity here

load('data/vehicle_rand_10.mat', 'x')
load('data/vehicle_rand_10.mat', 'y')

trainDynModel;

delta = 0.1;
z0 = zeros(1,4);
s0 = [0.1 0.1 0.1 0.1 0].^2;
zGP = zeros(31,4); zDyn = zeros(31,4); sGP = zeros(31,4);
zGP(1,:) = [z0]; zDyn(1,:) = [z0]; sGP(1,:) = s0(1:4);
s=diag(s0);

for i = 1:30
   [M, S] = dynmodel.fcn(dynmodel, [zGP(i,:) delta]', s);
   zGP(i+1,:) = [M]';
   zDyn(i+1,:) = simulate(zDyn(i,:), delta, plant);
   s(1:4,1:4) = S; sGP(i+1,:) = diag(S);
end

[zDyn zGP sGP]
