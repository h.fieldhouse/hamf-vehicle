%% reanimate.m
% *Summary:* Script to display animation for past dataset
%
%
%   function reanimate(X, dt)
%
%
% *Input arguments:*
%
%   X     dataset                                                   [5 x N]
%   dt    time step (seconds)                                       [1 x 1]
%   s     replay speed (1 = true speed, 2 = half speed, ...)        [1 x 1]
%
%
% Last modified by HAMF: 2021-10-18
%
%% High-Level Steps
% # Loop to recall draw function on past data

function reanimate(X, dt, s)
%% Code

% 1. Initialization
settings_vehicle;                      % load scenario-specific settings

T = dt*s;
tStep = 1:size(X,1);

Figure = figure(2); clf(2);
set(Figure, 'MenuBar', 'none');
set(Figure, 'ToolBar', 'none');
ax1 = nexttile;
plot(ax1,tStep,X(:,3))
ylabel(ax1,'y position (metres)')

ax2 = nexttile;
plot(ax2,tStep,X(:,5))
ylabel(ax2,'Steering angle (radians)')
xlabel(ax2,'Time step')

Figure = figure(1); clf(1);
set(Figure, 'MenuBar', 'none');
set(Figure, 'ToolBar', 'none');

draw_vehicle(X(1,3), X(1,4), X(1,end), cost);
waitforbuttonpress;

% 2. Loop

for n = tStep(2:end)
    draw_vehicle(X(n,3), X(n,4), X(n,end), cost);
    pause(T);
end

end