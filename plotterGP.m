%% plotterGP.m
% *Summary:* Script to run a dynamics simulation
%
%
% *Input arguments:*
%
%
%
% Last modified by HAMF: 2021-10-19
%
%% High-Level Steps
% # Load dynmodel GP
% # Plot graphs using GP process test points

%load('data/dynmodel_SPGP_300of600rand.mat')

z0 = [0 0.5 0 0];
s0 = diag([0.1 0.1 0.1 0.1 0]);
maxU = 5;
N = 100;
z = zeros(N,4); s = zeros(N,4);

delta = linspace (-maxU,maxU,N);

for i = 1:N
    [M, S] = dynmodel.fcn(dynmodel, [z0 delta(i)]', s0);
    z(i,:) = M'; s(i,:) = diag(S);
end

Figure = figure(1); clf(1);
ax1 = nexttile;
plot(ax1,delta,z(:,2),'r','linewidth',2)
plot(ax1,delta,z(:,2)+sqrt(s(:,2)),'r','linewidth',0.5)
plot(ax1,delta,z(:,2)-sqrt(s(:,2)),'r','linewidth',0.5)
ylabel(ax1,'omega (rad/s)')

ax2 = nexttile;
plot(ax2,delta,z(:,1))
ylabel(ax2,'Lateral velocity (m/s)')
xlabel(ax2,'Steering Angle (radians)')
